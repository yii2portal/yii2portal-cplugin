<?php

namespace yii2portal\cplugin;

use Yii;
use yii\base\Event;
use yii\web\Response;
use yii2portal\cplugin\models\CPlugin;
use yii2portal\cplugin\models\CPluginException;

class Module extends \yii2portal\core\Module
{

    public $plugins = [
        'all'
    ];

    public $autorender = false;

    private $_loaded = [];

    public function init()
    {
        parent::init();

        if ($this->autorender) {
            Yii::$app->response->on(Response::EVENT_BEFORE_SEND, function (Event $Event) {
                $response = $Event->sender;
                if ($response->format === Response::FORMAT_HTML) {
                    if (!empty($response->data)) {
                        $response->data = $this->render($response->data);
                    }
                    if (!empty($response->content)) {
                        $response->content = $this->render($response->data);
                    }
                }
            });
        }
    }

    public function render($content)
    {
        return $this->parsePlugins($content);
    }

    public function remove($content)
    {
        return $this->parsePlugins($content, [], false);
    }

    public function parsePlugins($content, $types = [], $render = true)
    {
        $matches = array();
        preg_match_all('~({cplugin([^}]+)})~iUu', $content, $matches);
        $c = count($matches[1]);

        for ($i = 0; $i < $c; $i++) {
            $params = $this->parseParams($matches[2][$i]);

            $plugin_content = '';
            if (!empty($params['type']) && (empty($types) || in_array($params['type'], $types))) {
                try {
                    $plugin_content = '';
                    if($render) {
                        $plugin_content = $this->getCPlugin($params['type'])->pluginRender($params);
                    }
                } catch (CPluginException $e) {
                    Yii::warning($e->getMessage());
                }
            }

            $content = str_replace($matches[1][$i], $plugin_content, $content);
        }

        return $content;
    }

    public function parseParams($str)
    {

        $match = array();
        preg_match_all('~([a-z0-9_-]+)=(?:\'|")([^\'"]+)(?:\'|")~iuU', $str, $match);
        $i = count($match[1]);
        $params = array();
        while ($i--) {
            $paramValue = $match[2][$i];
            $paramValue = htmlspecialchars_decode($paramValue);
            $paramValue = preg_replace([
                "~&quot;~",
                "~&#039;~",
                "~&#123;~",
                "~&#125;~",
            ], [
                '"',
                "'",
                "{",
                "}"
            ], $paramValue);
            $params[$match[1][$i]] = $paramValue;
        }
        return $params;
    }

    /**
     * @param string $plugin Имя плагина
     * @return CPlugin Возвращает обьект CPlugin
     * @throws CPluginException
     */
    public function getCPlugin($plugin)
    {
        if (!isset($this->_loaded[$plugin])) {
            throw new CPluginException("{$plugin} not exists", CPluginException::NOTEXISTS);
        }
        return $this->_loaded[$plugin]['object'];
    }

    public function registerPlugin($className)
    {

        $class = new \ReflectionClass($className);
        $classParent = $class->getParentClass();
        if ($classParent && $classParent->getName() == CPlugin::className()) {

            $namespace = explode('\\', $class->getName());
            $plugin = strtolower(array_pop($namespace));
            $this->_loaded[$plugin] = array(
                'classname' => $className,
                'object' => Yii::createObject($className)
            );
        } else {
            throw new CPluginException("{$className} not implement CPlugin", CPluginException::NOTIMPLEMENT);
        }


    }


}