<?php

namespace yii2portal\cplugin\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii2portal\core\controllers\Controller;

class PreviewController extends Controller
{

    public $enableCsrfValidation = false;

    public function actionIndex()
    {
        $content = Yii::$app->request->post('content');

        if(Yii::$app->request->isPost && !empty($content)) {
            $this->layout = 'empty';
            return $this->render('index', [
                'content' => $content
            ]);
        }else{
            throw new NotFoundHttpException();
        }
    }



}
