<?php

namespace yii2portal\cplugin\models;

use yii\base\Exception;

class CPluginException extends Exception {

    const NOTEXISTS = 1;
    const NOTIMPLEMENT = 2;

    public function __construct($message = null, $code = null, $previous = null) {
        if ($previous == null)
            parent::__construct($message, $code);
        else
            parent::__construct($message, $code, $previous);
    }

}