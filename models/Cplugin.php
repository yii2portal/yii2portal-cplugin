<?php

namespace yii2portal\cplugin\models;

use Yii;
use yii\base\Widget;

abstract class Cplugin extends Widget{

    /**
     * Занимается прорисовкой формы настроек плагина для tinymce.
     * Возвращает следующий набор параметров:
     * array(
     *      'hmtl'=>''                  // HTML-форма для конфигурации
     *      'config'=> true | false     // true - плагин необходимо конфигурировать, false - плагин не конфигурируется,
     *      'width' => 100              // ширина устанавливаемая по-умолчанию
     *      'height' => 100             // высота устанавливаемая по-умолчанию
     *      'resizable' => true | false // можно ли изменять блоку размеры
     * )
     * @param array $params Данные для вставки в форму
     * @return array Данные для tinymce
     */
    public abstract function pluginConfig($params);

    /**
     * Занимается проверкой параметров плагина
     * Возвращает следующее:
     * array(
     *      'status'=>true|false,   // Статус проверки
     *      'messages'=>array(),    // Массив сообщений для tinymce
     *      'params'=>array()       // Массив отфильтрованных параметров,
     * )
     * @param array $params Массив параметров, пришедший из tinymce
     * @return array Массив параметров
     */
    public abstract function pluginChk($params);

    /**
     * Занимается прорисовкой уже самого html-блока на основе параметров плагина
     * @param array $params Параметры плагина
     * @return string HTML
     */
    public abstract function pluginRender($params);

    protected function getSize($size) {

        if (substr($size, -1, 1) != '%') {
            $size = intval($size) . "px";
        }
        return $size;
    }

    public function getViewPath()
    {
        $class = new \ReflectionClass($this);

        list(, $module) = explode('\\', $class->getNamespaceName());


        $viewPath = implode(DIRECTORY_SEPARATOR,[
            Yii::$app->getModule($module)->viewPath,
            "widgets",
            "cplugin"
        ]);



        if(is_dir($viewPath)){
            return $viewPath;
        }else{
            return parent::getViewPath();
        }
    }

}